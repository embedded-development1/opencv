import numpy as np
import cv2 as cv
import time

cap = cv.VideoCapture(0)
fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output.avi', fourcc, 30.0, (640, 480))
timeStart = time.time()
img1 = cv.imread('noroff.png')
upDirection = True
y = 450
steps = 0

if not cap.isOpened():
    print("Cannot open camera")
    exit()

while True:
    timeCurrent = time.time()
    seconds = timeCurrent - timeStart
    ret, frame = cap.read()

    if not ret:
        print("Cant recieve frame (stream end?) Exiting... ")
        break
    if(seconds < 11):
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        frame[:,:,0] = gray
        frame[:,:,1] = gray
        frame[:,:,2] = gray
    elif(seconds < 21):
        frame = cv.circle(frame, (120, 200), 80, (15, 253, 250), -1)
        frame = cv.circle(frame, (90, 180), 10, (23, 137, 201), -1)
        frame = cv.circle(frame, (150, 180), 10, (23, 137, 201), -1)
        frame = cv.ellipse(frame, (120, 210), (50, 50), 0, 0, 180, (23, 137, 201), -1)

        frame = cv.circle(frame, (520, 200), 80, (15, 253, 250), -1)
        frame = cv.circle(frame, (490, 180), 10, (23, 137, 201), -1)
        frame = cv.circle(frame, (550, 180), 10, (23, 137, 201), -1)
        frame = cv.ellipse(frame, (520, 210), (50, 50), 0, 0, 180, (23, 137, 201), 3)
        font = cv.FONT_HERSHEY_COMPLEX_SMALL
        frame = cv.putText(frame, 'Hello World', (40, y), font, 4, (255, 255, 255), 2)
        if(steps < 6 and upDirection is True):
            y = y - 1
            steps = steps + 1
        elif(steps > 0 and upDirection is False):
            y = y + 1
            steps = steps - 1
        else:
            if(upDirection == True):
                upDirection = False
            else:
                upDirection = True
    elif(seconds < 31):
        height, width, depth = frame.shape
        img1 = cv.resize(img1,(width, height))
        frame = cv.addWeighted(img1, 0.5, frame, 0.5, 0)
    elif(seconds < 41):
        copy = frame[200:250, 330:390]
        frame[190:240, 500:560] = copy
        frame[100:150, 50:110] = copy
        frame[400:450, 200:260] = copy
    elif(seconds < 51):
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        black = np.array([0, 0, 0])
        dark_gray = np.array([100, 100, 100])
        mask = cv.inRange(hsv, black, dark_gray)
        frame[mask>0]=(0,0,255)
    if(seconds > 61):
        break

    out.write(frame)
    cv.imshow('frame', frame)

    if cv.waitKey(1) == ord('q'):
        break

cap.release()
out.release()
cv.destroyAllWindows()